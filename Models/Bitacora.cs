﻿using System;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace GestionAsp.Models
{
    public class Bitacora
    {
        [Key]
        [Column(Order = 1)]
        public int Id { get; set; }
        [Key]
        [Column(Order = 2)]
        [DataType(DataType.Date)]
        [Display(Name = "Fecha")]
        public DateTime FechaAccion { get; set; }

        [Display(Name = "Acción")]
        public virtual AccionBitacora AccionBitacora { get; set; }

        [Display(Name = "Nombre del Equipo")]
        public string NombreEquipo { get; set; }

        [Display(Name = "Dominio")]
        public string Ntdom { get; set; }
    }

    public enum AccionBitacora
    {
        ALTA = 1,
        BAJA = 2,
        INGRESO = 3,
        EGRESO = 4
    }
}