﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Web;

namespace GestionAsp.Models
{
    public class Usuario
    {

        const string MAX_CARACTERES = "El usuario debe ser de 8 caracteres";

        public Usuario()
        {
            UsuariosAdministradoras = new List<UsuarioAdministradora>(3);
        }

        [Key]
        [Display(Name = "Usuario")]
        [StringLength(8, ErrorMessage = MAX_CARACTERES)]
        [Required]
        public string IdUsuario { get; set; }

        [Required, StringLength(15, ErrorMessage = "El máximo permitido son 15 caracteres"), Display(Name ="Contraseña")]
        [DataType(DataType.Password)]
        public string Contrasenia { get; set; }
        [Required]
        public string Nombre { get; set; }
        [Required]
        public string Apellido { get; set; }

        [EnumDataType(typeof(Nivel))]
        [Required]
        public Nivel Nivel { get; set; }


        public virtual List<Bitacora> Bitacora { get; set; }
        [Required]
        public virtual List<UsuarioAdministradora> UsuariosAdministradoras { get; set; }
    }

   
    public enum Nivel
    {
        [Required]
        Supervisor = 1,
        [Required]
        Operador = 2,
        [Required]
        Invitado = 3,
        [Required]
        Otro = 4,
        [Required]
        Administrador = 5

    }
}