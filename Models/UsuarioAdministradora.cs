﻿using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace GestionAsp.Models
{
    public class UsuarioAdministradora
    {
        public UsuarioAdministradora()
        {
        }

        public UsuarioAdministradora(Administradora administradora)
        {
            Administradora = administradora;
        }

        public UsuarioAdministradora(string idUsuarioAdm)
        {
            IdUsuarioAdm = idUsuarioAdm;
        }

        [Key]
        [Required]
        [Display(Name = "Usuario Marca")]
        [Column(Order = 1)]
        public string IdUsuarioAdm { get; set; }
        [Key]
        [Required]
        [Column(Order = 2)]
        public Administradora Administradora { get; set; }



    }


    public enum Administradora
    {
        [Required]
        Visa = 1,
        [Required]
        MasterCard = 2,
        [Required]
        [Display(Name = "American Express")]
        Amex = 3,
        Banelco = 4
    }
}