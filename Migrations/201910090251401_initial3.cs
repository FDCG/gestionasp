namespace GestionAsp.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class initial3 : DbMigration
    {
        public override void Up()
        {
            CreateTable(
                "dbo.Bitacoras",
                c => new
                    {
                        Id = c.Int(nullable: false),
                        FechaAccion = c.DateTime(nullable: false),
                        AccionBitacora = c.Int(nullable: false),
                        NombreEquipo = c.String(),
                        Ntdom = c.String(),
                        Usuario_IdUsuario = c.String(maxLength: 8),
                    })
                .PrimaryKey(t => new { t.Id, t.FechaAccion })
                .ForeignKey("dbo.Usuarios", t => t.Usuario_IdUsuario)
                .Index(t => t.Usuario_IdUsuario);
            
            CreateTable(
                "dbo.UsuarioAdministradoras",
                c => new
                    {
                        IdUsuarioAdm = c.String(nullable: false, maxLength: 128),
                        Administradora = c.Int(nullable: false),
                        Usuario_IdUsuario = c.String(maxLength: 8),
                    })
                .PrimaryKey(t => new { t.IdUsuarioAdm, t.Administradora })
                .ForeignKey("dbo.Usuarios", t => t.Usuario_IdUsuario)
                .Index(t => t.Usuario_IdUsuario);
            
            CreateTable(
                "dbo.Usuarios",
                c => new
                    {
                        IdUsuario = c.String(nullable: false, maxLength: 8),
                        Contrasenia = c.String(nullable: false, maxLength: 15),
                        Nombre = c.String(nullable: false),
                        Apellido = c.String(nullable: false),
                        Nivel = c.Int(nullable: false),
                    })
                .PrimaryKey(t => t.IdUsuario);
            
        }
        
        public override void Down()
        {
            DropForeignKey("dbo.UsuarioAdministradoras", "Usuario_IdUsuario", "dbo.Usuarios");
            DropForeignKey("dbo.Bitacoras", "Usuario_IdUsuario", "dbo.Usuarios");
            DropIndex("dbo.UsuarioAdministradoras", new[] { "Usuario_IdUsuario" });
            DropIndex("dbo.Bitacoras", new[] { "Usuario_IdUsuario" });
            DropTable("dbo.Usuarios");
            DropTable("dbo.UsuarioAdministradoras");
            DropTable("dbo.Bitacoras");
        }
    }
}
