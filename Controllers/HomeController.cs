﻿using GestionAsp.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace GestionAsp.Controllers
{
    public class HomeController : Controller
    {
        private readonly UsuarioContext db = new UsuarioContext();
        public ActionResult Index()
        {
            if (Session["Usuario"] == null)
            {
                return RedirectToAction("Login", "Home");
            }
            return View();
        }

        public ActionResult About()
        {
            ViewBag.Message = "Your application description page.";

            return View();
        }

        public ActionResult Contact()
        {
            ViewBag.Message = "Your contact page.";

            return View();
        }

        public ActionResult Login()
        {
            if (Session["Usuario"]!= null)
            {
                return RedirectToAction("Index", "Home");
            }
            return View();
        }
        [HttpPost]
        public ActionResult Ingresar(string idUsuario, string contrasenia)
        {

            Usuario usuario;

            if (idUsuario == null || contrasenia == null)
            {
                return ViewBag.Error("Usuario y contraseña requeridos");
            }

            usuario = db.Usuarios.Find(idUsuario);
            if (usuario == null)
            {
                return ViewBag.Error("El usuario no existe");
            }

            if(usuario.IdUsuario == idUsuario && usuario.Contrasenia ==  contrasenia)
            {

                Session["Usuario"] = usuario.IdUsuario;
                

                return RedirectToAction("Index", "Home");
            }
            else
            {
                return ViewBag.Error = "Usuario y/o contraseña inválido";
            }

            
        }
    }
}