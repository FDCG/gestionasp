﻿using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Data.Entity.ModelConfiguration.Conventions;
using System.Linq;
using System.Threading.Tasks;
using GestionAsp.Models;

namespace GestionAsp.Models { 
    public class UsuarioContext : DbContext
    {

        public UsuarioContext() : base("DefaultConnection")
        {

        }

        public virtual DbSet<Usuario> Usuarios { get; set; }
        public virtual DbSet<UsuarioAdministradora> UsuarioAdministradoras { get; set; }
        public virtual DbSet<Bitacora> Bitacoras { get; set; }
        protected override void OnModelCreating(DbModelBuilder modelBuilder)
        {

            modelBuilder.Conventions.Remove<OneToManyCascadeDeleteConvention>();



        }

    }
}
